function onresize() {
  resizeCanvas(window.innerWidth, window.innerHeight);
};

var swing_frames = [];

// var frame_count = 181

var start_frame = 127
var length = 45

function preload(){
  for(var i = start_frame; i < start_frame + length; i++){
    // load images and add them to an array
    swing_frames.push(loadImage(`images/out${i}.png`))
  }
}

function setup() {
  var canvas;
  canvas = createCanvas();
  canvas.canvas.getContext("2d").imageSmoothingEnabled = false;
  onresize();
};

var current_frame = 0

var time = 0
var speed = 1

var cycle = 0

function draw(){
  // time += (deltaTime * 0.001) * speed

  // if(time >= 1){
  //   cycle = 1 - cycle
  // }

  // time = time - Math.floor(time)

  // current_frame = (current_frame + 1) % length

  // let t = 0.5 + 0.5 * Math.sin(frameCount * 0.06)

  // let x = Math.floor((mouseX / width) * length)


  // var t = current_frame / length

  // t = 0.5

  // t += (cycle == 0 ? -1 : 1) * (current_frame / length) * 0.5
  // t = Math.abs(cycle - time)


  // var sy = Math.min(length - 1, Math.floor(t * length))



  let normalized_y = mouseY / height
  let y = constrain(Math.floor((1 - normalized_y) * length), 0, length - 1)
  image(swing_frames[y], 0,0)

  
  // fill(255)
  // textSize(40)
  // text(time + " \n" + cycle, 0, 50)
}